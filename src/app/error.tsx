"use client";

import { useRouter } from "next/navigation";

export default function Error() {
  const router = useRouter();

  return (
    <>
      <p>Error while loading</p>
      <button onClick={router.refresh}>Try again</button>
    </>
  );
}
