"use client";

import { useState } from "react";

import TodoList from "@/components/TodoList";
import Navigation from "@/components/Navigation";

export default function HomePage() {
  const [editMode, setEditMode] = useState<boolean>(false);

  return (
    <>
      <header className="flex justify-between items-center mb-4">
        <h1 className="text-2xl">Todos</h1>
        <Navigation onToggleEdit={() => setEditMode(!editMode)} />
      </header>
      <TodoList isEditMode={editMode} />
    </>
  );
}
