import Link from "next/link";

export default function Navigation({ onToggleEdit }: any) {
  return (
    <nav className="flex gap-1 justify-between items-center">
      <Link
        className="border border-slate-300 text-slate-300 px-2 py-1 rounded hover:bg-slate-700 focus-within:bg-slate-700 outline-none"
        href="/createTodo"
      >
        Create new
      </Link>
      <button
        onClick={onToggleEdit}
        className="border border-slate-300 text-slate-300 px-2 py-1 rounded hover:bg-slate-700 focus-within:bg-slate-700 outline-none"
      >
        Edit
      </button>
    </nav>
  );
}
