"use client";

import { deleteOne } from "@/lib/delete";

type TodoItemProps = {
  id: string;
  title: string;
  complete: boolean;
  toggleTodo: (id: string, complete: boolean) => void;
  isEditMode: boolean;
  onDelete: (id: string) => void;
};

export default function TodoItem({
  id,
  title,
  complete,
  toggleTodo,
  isEditMode,
  onDelete,
}: TodoItemProps) {
  return (
    <li className="flex justify-between">
      <div className="flex gap-1 items-center">
        <input
          id={id}
          type="checkbox"
          className="cursor-pointer peer"
          defaultChecked={complete}
          onChange={(e) => toggleTodo(id, e.target.checked)}
        />
        <label
          htmlFor={id}
          className="peer-checked:line-through peer-checked:text-slate-500 cursor-pointer"
        >
          {title}
        </label>
      </div>
      {isEditMode && (
        <button
          onClick={() => onDelete(id)}
          className="border border-slate-300 text-slate-300 px-2 py-1 rounded hover:bg-slate-700 focus-within:bg-slate-700 outline-none"
        >
          x
        </button>
      )}
    </li>
  );
}
