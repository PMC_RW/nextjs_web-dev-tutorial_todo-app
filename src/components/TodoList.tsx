"use client";
import { useEffect, useState } from "react";
import Loading from "@/app/loading";

import { readAll } from "@/lib/readAll";
import { toggleComplete } from "@/lib/toggleComplete";
import { Todo } from "@/models/todo.model";
import TodoItem from "./TodoItem";
import { deleteOne } from "@/lib/delete";

export default function TodoList({ isEditMode }: any) {
  const [todos, setTodos] = useState<Todo[]>([]);
  useEffect(() => {
    async function getData() {
      setTodos(await readAll());
    }

    getData();
  }, []);

  function handleDelete(id: string) {
    deleteOne(id);
  }

  return (
    <>
      <ul className="pl-4">
        {todos.length > 0 ? (
          todos.map((todo) => (
            <TodoItem
              key={todo.id}
              {...todo}
              toggleTodo={toggleComplete}
              isEditMode={isEditMode}
              onDelete={handleDelete}
            />
          ))
        ) : (
          <Loading />
        )}
      </ul>
    </>
  );
}
