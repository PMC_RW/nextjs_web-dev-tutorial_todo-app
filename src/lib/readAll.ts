"use server";

import { prisma } from "@/db";

export async function readAll() {
  return await prisma.todo.findMany();
}
