"use server";

import { redirect } from "next/navigation";
import { prisma } from "@/db";

export async function create(data: FormData) {
  const title = String(data.get("title")?.valueOf());
  if (title.length > 0) {
    await prisma.todo.create({ data: { title, complete: false } });

    redirect("/");
  }
}
