"use server";

import { prisma } from "@/db";

export async function deleteOne(id: string) {
  await prisma.todo.delete({ where: { id } });
}
